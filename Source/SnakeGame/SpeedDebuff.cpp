// Fill out your copyright notice in the Description page of Project Settings.


#include "SpeedDebuff.h"
#include "snake.h"

// Sets default values
ASpeedDebuff::ASpeedDebuff()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASpeedDebuff::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASpeedDebuff::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASpeedDebuff::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnake>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeSpeedDebuff();
			Destroy();
		}
	}
}

